# ORID

## O

1. Today I learned concepts related to integration testing, such as how to test in Springboot
2. Then learned annotations like @SpringBootTest @AutoConfigureMockMvc, @BeforeEach
3. Later exercises learned how to add service layers, such as how to split the business

## R

substantiate

## I

1. In the exercise, there were some concepts that I hadn't encountered before, such as mock, and some methods such as give(), willReturn(), verify(), etc., which I've never seen before, and I had trouble using them.
2. I feel that conducting tests requires a pre-validated mindset that can predict the outcome and use the relevant api wisely, and this aspect is a major difficulty

## D

​	Going forward, I plan to continue practicing integration testing and refining my understanding of software architecture. I also plan to explore other testing frameworks and approaches to see how they compare to Spring Boot's integration testing.