package com.thoughtworks.springbootemployee.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    MockMvc client;

    @BeforeEach
    void cleanEmployeeList() {
        companyRepository.clearAll();
        employeeRepository.clearAll();
    }

    @Test
    void should_return_companies_when_getCompanies_given_companiess() throws Exception {
        // Given
        Company springCompany = new Company(null, "spring");
        Company bootCompany = new Company(null, "boot");
        companyRepository.insert(springCompany);
        companyRepository.insert(bootCompany);
        // When
        client.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(springCompany.getId()))
                .andExpect(jsonPath("$[0].name").value(springCompany.getName()))
                .andExpect(jsonPath("$[1].id").value(bootCompany.getId()))
                .andExpect(jsonPath("$[1].name").value(bootCompany.getName()))
        ;
    }

    @Test
    void should_return_specific_company_when_find_company_by_id_given_company_id() throws Exception {
        // Given
        Company springCompany = new Company(null, "spring");
        companyRepository.insert(springCompany);
        // When, Then
        client.perform(MockMvcRequestBuilders.get("/companies/{id}", springCompany.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(springCompany.getName()));
    }

    @Test
    void should_return_employees_of_the_company_when_getEmployeesByCompanyId_given_company_id() throws  Exception {
        // Given
        Company springCompany = new Company(null, "spring");
        companyRepository.insert(springCompany);

        Employee john = new Employee(null, "John Smith", 32, "Male", 5000.0);
        Employee jane = new Employee(null, "Jane Johnson", 28, "Female", 6000.0);
        Employee david = new Employee(null, "David Williams", 35,"Male", 5500.0);
        Employee emily = new Employee(null, "Emily Brown", 23, "Female", 4500.0);
        Employee michael = new Employee(null, "Michael Jones", 40, "Male", 7000.0);
        john.setCompanyId(springCompany.getId());
        jane.setCompanyId(2L);
        david.setCompanyId(2L);
        emily.setCompanyId(springCompany.getId());
        michael.setCompanyId(2L);
        employeeRepository.insert(john);
        employeeRepository.insert(jane);
        employeeRepository.insert(david);
        employeeRepository.insert(emily);
        employeeRepository.insert(michael);

        client.perform(MockMvcRequestBuilders.get("/companies/{id}/employees", springCompany.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(john.getId()))
                .andExpect(jsonPath("$[0].name").value(john.getName()))
                .andExpect(jsonPath("$[1].id").value(emily.getId()))
                .andExpect(jsonPath("$[1].name").value(emily.getName()))
        ;

    }

    @Test
    void should_return_companies_in_page_when_findByPage_given_page_and_size() throws Exception {
        Company springCompany = new Company(null, "spring");
        Company bootCompany = new Company(null, "boot");
        Company javaCompany = new Company(null, "java");
        Company pythonCompany = new Company(null, "python");
        Company cCompany = new Company(null, "c");
        companyRepository.insert(springCompany);
        companyRepository.insert(bootCompany);
        companyRepository.insert(javaCompany);
        companyRepository.insert(pythonCompany);
        companyRepository.insert(cCompany);

        client.perform(MockMvcRequestBuilders.get("/companies?page={page}&size={size}",1,3))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").value(springCompany.getId()))
                .andExpect(jsonPath("$[0].name").value(springCompany.getName()))
                .andExpect(jsonPath("$[1].id").value(bootCompany.getId()))
                .andExpect(jsonPath("$[1].name").value(bootCompany.getName()))
                .andExpect(jsonPath("$[2].id").value(javaCompany.getId()))
                .andExpect(jsonPath("$[2].name").value(javaCompany.getName()))
        ;

    }

    @Test
    void should_return_created_company_when_insert_company_given_company_json() throws Exception {
        // Given
        Company springCompany = new Company(null, "spring");
        String springCompanyJson = new ObjectMapper().writeValueAsString(springCompany);
        // When
        client.perform(MockMvcRequestBuilders.post("/companies")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(springCompanyJson)
                )    // Then
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name").value(springCompany.getName()))
        ;
    }

    @Test
    void should_return_updated_company_when_update_company_given_updated_company_json_and_company_id() throws Exception {
        Company company = new Company(null, "spring");
        companyRepository.insert(company);

        Company updatedCompany = new Company(null, "spring boot");
        String updatedCompanyJson = new ObjectMapper().writeValueAsString(updatedCompany);

        client.perform(MockMvcRequestBuilders.put("/companies/{id}", company.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedCompanyJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(updatedCompany.getName()))
        ;

    }

    @Test
    void should_return_no_content_when_delete_company_given_company_id() throws Exception {
        Company company = new Company(null, "spring");
        companyRepository.insert(company);

        client.perform(MockMvcRequestBuilders.delete("/companies/{id}", company.getId()))
                .andExpect(status().isNoContent());
    }

}

