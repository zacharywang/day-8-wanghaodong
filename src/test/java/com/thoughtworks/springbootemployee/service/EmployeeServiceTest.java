package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.AgeIsInvalidException;
import com.thoughtworks.springbootemployee.exception.CanNotUpdateInactiveEmployeeException;
import com.thoughtworks.springbootemployee.exception.SalaryNotMatchAgeException;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import com.thoughtworks.springbootemployee.service.EmployeeService;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

class EmployeeServiceTest {
    @Test
    void should_not_create_successfully_when_create_employee_given_age_is_invalid() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "sj", 15, "Female", 6000d);
        //given(employeeRepository.insert(any())).willReturn(employee);
        assertThrows(AgeIsInvalidException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_not_create_successfully_when_create_employee_given_salary_not_match_age() {
        EmployeeService employeeService = new EmployeeService(mock(EmployeeRepository.class));
        Employee employee = new Employee(1L, "sj", 33, "Female", 6000d);
        //given(employeeRepository.insert(any())).willReturn(employee);
        assertThrows(SalaryNotMatchAgeException.class, () -> employeeService.create(employee));
    }

    @Test
    void should_create_successfully_when_create_employee_given_age_is_valid() {
        EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
        Employee employee = new Employee(1L, "dz", 20, "Male", 6000d);
        given(employeeRepository.insert(any())).willReturn(employee);

        EmployeeService employeeService = new EmployeeService(employeeRepository);
        Employee createdEmployee = employeeService.create(employee);

        assertThat(createdEmployee.getId()).isEqualTo(1);
        assertThat(createdEmployee.getName()).isEqualTo("dz");
        assertThat(createdEmployee.getSalary()).isEqualTo(6000);
        assertThat(createdEmployee.getGender()).isEqualTo("Male");

        verify(employeeRepository).insert(argThat(employee1 -> {
            assertThat(createdEmployee.getId()).isEqualTo(1);
            assertThat(createdEmployee.getName()).isEqualTo("dz");
            assertThat(createdEmployee.getSalary()).isEqualTo(6000);
            assertThat(createdEmployee.getGender()).isEqualTo("Male");
            return true;
        }));
    }


    @Test
    void should_set_employee_inactive_when_delete_employee_given_an_active_employee() {
        // Given
        EmployeeRepository mock = mock(EmployeeRepository.class);
        EmployeeService employeeService = new EmployeeService(mock);
        Employee employee = new Employee(1L, "sj", 33, "Female", 6000d);
        employee.setActive(true);
        given(mock.findById(1L)).willReturn(employee);

        // When
        employeeService.delete(1L);

        // EmployeeService employeeServiceMock = mock(EmployeeService.class);
        verify(mock, times(1))
                .update(anyLong(),any());
    }

    @Test
    void should_update_successfully_when_update_employee_given_active_employee() {
        EmployeeRepository employeeMockRepository = mock(EmployeeRepository.class);
        Employee activeEmployee = new Employee(1L, "Lily", 25, "Female", 4000.0);
        activeEmployee.setActive(true);
        given(employeeMockRepository.findById(1L)).willReturn(activeEmployee);
        Employee employeeToUpdate = new Employee(1L, "Lily", 30, "Female", 5000.0);

        given(employeeMockRepository.update(anyLong(), any())).willReturn(employeeToUpdate);

        EmployeeService employeeService = new EmployeeService(employeeMockRepository);
        Employee updatedEmployee = employeeService.update(1L, employeeToUpdate);

        assertThat(updatedEmployee.getAge()).isEqualTo(30);
        assertThat(updatedEmployee.getSalary()).isEqualTo(5000.0);
        verify(employeeMockRepository).update(eq(1L), any(Employee.class));
    }

    @Test
    void should_throw_can_not_update_inactive_employee_exception_when_update_employee_given_inactive_employee() {
        EmployeeRepository mockRepository = mock(EmployeeRepository.class);
        Employee inactiveEmployee = new Employee(1L, "Lily", 25, "Female", 4000.0);
        inactiveEmployee.setActive(false);
        given(mockRepository.findById(1L)).willReturn(inactiveEmployee);

        EmployeeService employeeService = new EmployeeService(mockRepository);
        Employee employeeToUpdate = new Employee(1L, "Lily", 30, "Female", 5000.0);

        assertThrows(CanNotUpdateInactiveEmployeeException.class, () -> employeeService.update(1L, employeeToUpdate));
    }

}