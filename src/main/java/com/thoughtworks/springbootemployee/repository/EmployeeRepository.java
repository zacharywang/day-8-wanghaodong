package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.model.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import com.thoughtworks.springbootemployee.exception.EmployeeIdNotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Repository;

@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();
    private static AtomicLong atomicLongId = new AtomicLong(0);

    public Employee insert(Employee employee) {
        employee.setId(atomicLongId.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElseThrow(EmployeeIdNotFoundException::new);
    }

    public List<Employee> findByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee update(Long id, Employee employeeToUpdate) {
        employees.stream()
                .filter(employee1 -> Objects.equals(employee1.getId(), id))
                .forEach(employee -> BeanUtils.copyProperties(employee, employeeToUpdate));
        return employeeToUpdate;
    }
    public void delete(Long id){
        Employee toRemovedEmployee = employees.stream()
                .filter(employee -> Objects.equals(employee.getId(), id))
                .findFirst()
                .get();
//        toRemovedEmployee.setActive(false);
        employees.remove(toRemovedEmployee);
    }
    public List<Employee> findByPage(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());

    }
    public List<Employee> getEmployeeListByCompanyId(Long companyId) {
        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

    public void clearAll() {
        employees.clear();
    }
}
