package com.thoughtworks.springbootemployee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({
            AgeIsInvalidException.class,
            CanNotDeleteAnInactiveEmployeeException.class,
            CanNotUpdateInactiveEmployeeException.class,
            CompanyIdNotFoundException.class,
            EmployeeIdNotFoundException.class,
            SalaryNotMatchAgeException.class})
    public ResponseEntity<String> handleBadRequestException(Exception e) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
    }
}