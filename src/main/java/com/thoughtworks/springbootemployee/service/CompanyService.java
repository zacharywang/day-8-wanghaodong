package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.CompanyIdNotFoundException;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public Company insert(Company newCompany) {
        return companyRepository.insert(newCompany);
    }

    public List<Company> getAll() {
        return companyRepository.getAll();
    }

    public Company getCompanyById(Long companyId) {
        return companyRepository.getCompanyById(companyId);
    }

    public Company updateById(Long id, Company sourceCompany) {
        return companyRepository.updateById(id, sourceCompany);
    }

    public Company deleteById(Long id) {
        return companyRepository.deleteById(id);
    }

    public List<Employee> getEmployeeList(Long companyId) {
        if (companyId == null) {
            throw new CompanyIdNotFoundException();
        }
//        return companyRepository.getEployeeList(companyId);
        return employeeRepository.getEmployeeListByCompanyId(companyId);
    }

    public List<Company> getCompanyListByPage(int page, int size) {
        return companyRepository.getCompanyListByPage(page, size);
    }

}
