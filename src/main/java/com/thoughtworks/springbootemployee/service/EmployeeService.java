package com.thoughtworks.springbootemployee.service;

import com.thoughtworks.springbootemployee.exception.*;
import com.thoughtworks.springbootemployee.model.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {
    private EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer page, Integer size){
        return employeeRepository.findByPage(page,size);
    }

    public Employee insert(Employee newEmployee) {
        return employeeRepository.insert(newEmployee);
    }

    public Employee update(Long id, Employee employee) {
        Employee employeeToUpdate = employeeRepository.findById(id);
        if (employeeToUpdate == null) {
            throw new EmployeeIdNotFoundException();
        }
        if (!employeeToUpdate.isActive()) {
            throw new CanNotUpdateInactiveEmployeeException();
        }
        employeeToUpdate.merge(employee);
        return employeeRepository.update(id,employeeToUpdate);
    }

    public void delete(Long id) {
        Employee toRemovedEmployee = findById(id);
        if (toRemovedEmployee == null) {
            throw new EmployeeIdNotFoundException();
        }
        if (!toRemovedEmployee.isActive()) {
            throw new CanNotDeleteAnInactiveEmployeeException();
        }
        toRemovedEmployee.setActive(false);
        employeeRepository.update(id,toRemovedEmployee);
    }

    public void clearAll() {
        employeeRepository.clearAll();
    }

    public Employee create(Employee newEmployee) {
        if (!newEmployee.isValid()) {
            throw new AgeIsInvalidException();
        }
        if (!newEmployee.isSalaryMatchAge()) {
            throw new SalaryNotMatchAgeException();
        }
        newEmployee.setActive(true);
        return employeeRepository.insert(newEmployee);
    }
}
